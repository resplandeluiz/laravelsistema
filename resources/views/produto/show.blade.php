
@extends('layout.app')

@section('title',$produto->titulo)

@section('content')


	<h1> Produto {{$produto->titulo}} </h1>

	<ul>
	<div class="row">
		<div class="col-md-6 col-md-3">
	
		<li>Referencia {{$produto->referencia}}</li>
		<li>Preço R${{number_format($produto->preco,2,',','.')}}</li>
		<li>Adicionado em {{date("d/m/Y",strtotime($produto->created_at))}}</li>

	</ul>

	<p>{{$produto->descricao}} </p>
	</div>
	@if(file_exists('./img/produtos/' . md5($produto->id). 'jpg'))
	<div class="foto">
		
		{{Html::image(asset('img/produtos/'. md5($produto -> id).'jpg'))}}
		
	</div>
	@endif
	
	<a href='/produtos'>Voltar</a>


</div>
@endsection